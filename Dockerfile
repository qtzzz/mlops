FROM python:3.8-slim
ENV MLFLOW_TRACKING_URI='http://34.140.13.253:8000/'
WORKDIR /app
COPY requirements-prod.txt requirements.txt
RUN pip3 install -r requirements.txt
COPY . .
CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0", "--port=8080"]