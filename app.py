from flask import Flask, render_template, flash, request, redirect, url_for, send_from_directory
from mlflow.tracking.client import MlflowClient
import os
import computation.models
from werkzeug.utils import secure_filename


UPLOAD_FOLDER = '/tmp'
ALLOWED_EXTENSIONS = {'csv', 'tsv'}

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/', methods=['GET', 'POST'])
def index():

    client = MlflowClient()
    models = []
    for r in client.list_registered_models():
        models.append(str(r.name))
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            result = computation.models.run_calc(str(os.path.join(app.config['UPLOAD_FOLDER'], filename)), str(request.form.get('models')))
            # session['filename'] = result
            return redirect(url_for('download_file', name=result))
    return render_template("index.html", models=models)

# @app.route('/results')
# def results():
#     filename = request.args['filename'] 
#     return render_template("download.html",filename=filename)

# @app.route('/results/<filename>')
# def results_download(result):
#     return send_from_directory('/tmp/'+result)

@app.route('/uploads/<name>')
def download_file(name):
    return send_from_directory(
        app.config['UPLOAD_FOLDER'], name, as_attachment=True
    )
