import re
import pandas as pd
import matplotlib.pyplot as plt, matplotlib.image as mpimg
from sklearn.model_selection import train_test_split
from sklearn import svm
import mlflow
# import tools.s3
# import dvc.api


def clean_datas(labeled_images):
    images = labeled_images.iloc[0:5000,1:]
    labels = labeled_images.iloc[0:5000,:1]
    train_images, test_images,train_labels, test_labels = train_test_split(images, labels, train_size=0.8, random_state=0)
    test_images[test_images>0]=1
    train_images[train_images>0]=1
    return train_images, test_images,train_labels, test_labels

def reshape_test(test):
    test[test>0]=1
    return test

if __name__ == "__main__":
    mlflow.start_run()
    mlflow.sklearn.autolog()
    X_train, X_test, Y_train, Y_test  = clean_datas(pd.read_csv('data/train.csv'))
    
    clf = svm.SVC()
    clf.fit(X_train, Y_train.values.ravel())
    clf.score(X_test,Y_test)
    mlflow.sklearn.save_model(sk_model=clf, path='/tmp/svm')
    mlflow.end_run()
    
    
    
    