# MLOPS demo



List of tools used:
- [Gitlab-ci](https://docs.gitlab.com/ee/ci/)
- [Google cloud storage](https://cloud.google.com/storage)
- [Google kubernetes engine](https://cloud.google.com/kubernetes-engine)
- [MlFlow](https://mlflow.org/docs/latest/index.html)
- [DVC](https://dvc.org/doc)

---

## Context
This project aims to deliver an implementation of a combination of workflow and tools for an example ML project from notebook to production deployment

The base use case is answering to the kaggle challenge [digit recognizer](https://www.kaggle.com/c/digit-recognizer)

---

## Content
In this project you will find:
- Two notebooks (Which you can find the sources for in [source](##sources))
- A data folder containing the dvc file for the training dataset
- A packaged version of the models with mlflow code
- A very simple flask web app which:
    - Retrieve models list from mlflow
    - Serve a simple web page with jinja
    - Allows you to input your test dataset
    - Allows you to chose a model to run predictions with
    - Loads selected model from mlflow (Only models tagged for production tho!) 
    - Run predictions with selected model
    - Returns a ```.csv``` file containing the predictions and submittable to kaggle
- A Dockerfile of the app
- A ```.gitlab-ci.yml``` file for:
    - Building the image of the app and pushing it to a [Google Container Registry](https://cloud.google.com/container-registry)
    - Deploying the app on google kubernetes engine
- ```clean.sh```, a script to clean local directories in which the models are stored after training
- The ```requirements.txt``` file 

What is part of the project but not in this repository:
- The configuration of MlFlow and DVC which both work with a Google cloud bucket as a backend
- The training data used (which you can find [here](https://www.kaggle.com/c/digit-recognizer/data?select=train.csv))

---

## Sources: 
- https://github.com/TeFuirnever/Kaggle-Digit-Recognizer
- https://www.kaggle.com/archaeocharlie/a-beginner-s-approach-to-classification
