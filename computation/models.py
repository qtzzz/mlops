import pandas as pd
import models.cnn
import models.svm
import numpy as np
import pandas as pd
from datetime import datetime
import mlflow


filepath = '/tmp/'
def calc_cnn(input_file) -> str:
    """

    """
    now = datetime.now()
    curdate = now.strftime("%d-%m-%Y_%H-%M")
    path= filepath + 'result_' + curdate + '.csv'
    model = mlflow.pyfunc.load_model(model_uri=f"models:/CNN/Production")  
    df = pd.read_csv(input_file)
    result = model.predict(models.cnn.reshape_test(df))
    result = np.argmax(result,axis = 1)
    result = pd.Series(result,name="Label")
    submission = pd.concat([pd.Series(range(1,28001),name = "ImageId"),result],axis = 1)
    submission.to_csv(path ,index=False)
    return path.strip(filepath)

def calc_svm(input_file) -> str:
    now = datetime.now()
    curdate = now.strftime("%d-%m-%Y_%H-%M")
    path= filepath + 'result_' + curdate + '.csv'
    model = mlflow.pyfunc.load_model(model_uri=f"models:/SVM/Production")
    df = pd.read_csv(input_file)
    results=model.predict(models.svm.reshape_test(df)[0:5000])
    submission = pd.DataFrame(results)
    submission.index.name='ImageId'
    submission.index+=1
    submission.columns=['Label']
    submission.to_csv(path , header=True)
    return path.strip(filepath)

def run_calc(input_file, model:str ):
    if model == 'CNN':
        return calc_cnn(input_file)
    elif model == 'SVM':
        return calc_svm(input_file)
    # return 'result.csv'
